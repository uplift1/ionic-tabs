FROM node:14-slim as build

RUN yarn global add ionic
WORKDIR /opt
COPY app .
RUN yarn
RUN ionic build --prod

FROM nginx

EXPOSE 80

WORKDIR /usr/share/nginx/html

COPY --from=build /opt/www .
COPY config/nginx.conf /etc/nginx/conf.d/default.conf
